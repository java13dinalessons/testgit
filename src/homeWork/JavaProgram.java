package homeWork;


import java.util.*;

public class JavaProgram {
	
	public static String programName;
	public static int rankPlaceWorldwide;
	public static double version;
	public static boolean isItNewVesion;	
	

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		  System.out.println("Enter program name:");
		  JavaProgram.programName = myScanner.nextLine();
		  
		  System.out.println("Enter rank place worldwide");
		  JavaProgram.rankPlaceWorldwide = Integer.parseInt ( myScanner.nextLine());
		  
		  System.out.println("Enter version:");
		  JavaProgram.version = Double.parseDouble(myScanner.nextLine());
		  
		  System.out.println("Is it new version?");
		  JavaProgram.isItNewVesion = Boolean.parseBoolean(myScanner.nextLine());
		  
		  myScanner.close(); 
		  show();
		
		
			}

	public static void show() {
		System.out.println("Program Name:" +  JavaProgram.programName );
		System.out.println("Rank place:" +JavaProgram.rankPlaceWorldwide );
		System.out.println("Version:" +JavaProgram.version );
		System.out.println("Is it a new version? :" +JavaProgram.isItNewVesion );
		
}

	
}